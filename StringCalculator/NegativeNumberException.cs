﻿using System;

namespace StringCalculator
{
    internal class NegativeNumberException : Exception
    {
        public NegativeNumberException(string message) : base(message)
        {
        }
    }
}