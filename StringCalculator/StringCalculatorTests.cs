﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace StringCalculator
{
    [TestFixture]
    class StringCalculatorTests
    {
        [Test]
        public void addString_EmtyString_0()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("");

            Assert.AreEqual(0, result);
        }

        [Test]
        public void addString_1_1()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("1");

            Assert.AreEqual(1, result);
        }

        [Test]
        public void addString_2_2()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("2");

            Assert.AreEqual(2, result);
        }

        [Test]
        public void addString_1and2_3()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("1,2");

            Assert.AreEqual(3, result);
        }

        [Test]
        public void addString_1and2and3_6()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("1,2,3");

            Assert.AreEqual(6, result);
        }

        [Test]
        public void addString_newLineInInput_Ok()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("1\n2,3");

            Assert.AreEqual(6, result);
        }

        [Test]
        public void addString_CommaWithNewLine_InvalidInputExceptionThrown()
        {
            StringCalculator calculator = new StringCalculator();

            Assert.Throws<InvalidInputException>(() => calculator.add("1,\n2,3"));
        }

        [Test]
        public void addString_newLineWithComma_InvalidInputExceptionThrown()
        {
            StringCalculator calculator = new StringCalculator();

            Assert.Throws<InvalidInputException>(() => calculator.add("1\n,2,3"));
        }

        [Test]
        public void addString_newDelimiter_Ok()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("#;\n2;3");

            Assert.AreEqual(5, result);
        }

        [Test]
        public void addString_negativeNumber_negativeNumberExceptionThrows()
        {
            StringCalculator calculator = new StringCalculator();

            Assert.Throws<NegativeNumberException>(() => calculator.add("-1"), "Negatives not allowed: "+(-1));
        }

        [Test]
        public void addString_1001_0()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("1001");

            Assert.AreEqual(0, result);
        }

        [Test]
        public void addString_1and1001_1()
        {
            StringCalculator calculator = new StringCalculator();

            int result = calculator.add("1,1001");

            Assert.AreEqual(1, result);
        }
    }
}
