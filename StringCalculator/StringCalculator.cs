﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator
{
    class StringCalculator
    {
        private char delimiter;
        private string withoutNewDelimiter;

        internal int add(string input)
        {
            if (input.Contains(",\n") || input.Contains("\n,"))
            {
                throw new InvalidInputException();
            }
            if (input == "")
            {
                return 0;
            }

            handleNewDelimiter(input);

            if (withoutNewDelimiter.Contains(delimiter) || withoutNewDelimiter.Contains("\n"))
            {
                return handleALotOfNumbers();
            }

            return handleOneNumber();
        }

        private int handleOneNumber()
        {
            int number = Int32.Parse(withoutNewDelimiter);
            if (number < 0)
            {
                throw new NegativeNumberException("Negatives not allowed: " + number);
            }
            if (number > 1000)
            {
                return 0;
            }
            return number;
        }

        private int handleALotOfNumbers()
        {
            string[] terms = withoutNewDelimiter.Split(new Char[2] { delimiter, '\n' });
            int sum = 0;
            for (int i = 0; i < terms.Length; i++)
            {
                if (Int32.Parse(terms[i]) <= 1000)
                {
                    sum += Int32.Parse(terms[i]);
                }
            }
            return sum;
        }

        private void handleNewDelimiter(string input)
        {
            withoutNewDelimiter = input;
            delimiter = ',';
            if (input.StartsWith("#"))
            {
                delimiter = input[1];
                withoutNewDelimiter = input.Substring(3);
            }
        }
    }
}
